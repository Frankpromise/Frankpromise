# Hi there, I'm [Frank Promise Edah] 👋

## 👨‍💻 About Me

- 🎓 I'm a [Business and management] graduate from [university of Torino, Italy].
- 💼 I'm currently working as a [Site reliability/DevOps Engineer]
- 🌱 I'm currently learning [Kubernetes, Hashicorp Vault].
- 🎯 My primary focus is on [AWS, gitlab, Jenkins, Docker, Terraform, Flux, Helm].
- ⚡ Fun fact: [I am a proactive learner who enjoys learning and working with new technologies. I also volunteer to mentor people who are looking for a career change].

## 🛠️ Skills and Technologies

![AWS](https://img.shields.io/badge/-AWS-333333?style=flat&logo=amazon-aws&logoColor=FF9900)
![GitLab CI](https://img.shields.io/badge/-GitLab_CI-333333?style=flat&logo=gitlab&logoColor=FCA121)
![Docker](https://img.shields.io/badge/-Docker-333333?style=flat&logo=docker&logoColor=2496ED)
![Terraform](https://img.shields.io/badge/-Terraform-333333?style=flat&logo=terraform&logoColor=7B42BC)
![Kubernetes](https://img.shields.io/badge/-Kubernetes-333333?style=flat&logo=kubernetes&logoColor=326CE5)
![Python](https://img.shields.io/badge/-Python-333333?style=flat&logo=python&logoColor=3776AB)
![Bash](https://img.shields.io/badge/-Bash-333333?style=flat&logo=gnu-bash&logoColor=4EAA25)
![Jenkins](https://img.shields.io/badge/-Jenkins-333333?style=flat&logo=jenkins&logoColor=D24939)

## 📊 GitLab Activity

Visit my [GitLab profile](https://gitlab.com/Frankpromise) to see my activity and contribution graph.

### Project 1
- 🔗 [Repository](https://gitlab.com/Frankpromise/terraform_modules)
- 📚 This project contains all terraform modules i have built over time while learning. Its no way suitable for production. However, perfect for learning.

### Project 2
- 🔗 [Repository](https://gitlab.com/Frankpromise/cicd_for_aws)
- 📚 This project contains a basic EKS cluster spinned up in AWS. Not suitable for production, but good for testing purposes.



## 🤝 Connect with me

Include links to your social media profiles, personal website, or portfolio.

[![LinkedIn](https://img.shields.io/badge/-LinkedIn-333333?style=flat&logo=LinkedIn&logoColor=0077B5)](https://www.linkedin.com/in/promise-edah-frank-959673159/)
